<?php
// $Id$
/**
 * @file
 * Admin page callback fot the meebo settings form. 
 */

/**
 * Build and returns the meebo settings form.
 */
function meebo_settings($form, &$form_state) {
  $form = array();
  
  $form['meebo_bar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Meebo Bar'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  $form['meebo_bar']['meebo_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Meebo Network Id'),
    '#description' => t('Your meebo network id looks like this: <b>mysite_pu68fu</b>. 
			If you don\'t have a meebo account you can <a target="_blank" href="https://bar.meebo.com/setup/1/">apply now</a>.'),
    '#default_value' => variable_get('meebo_id', ''),
    '#size' => 30,
    '#required' => TRUE,
  );
  
  $form['meebo_bar']['meebo_disable_share_button'] = array(
    '#title' => t('Hide share page button'),
    '#type' => 'checkbox',
    '#description' => t('Hide the Share Page button on the bar'),    
    '#default_value' => variable_get('meebo_disable_share_button', FALSE),
  );
  
  $form['meebo_bar']['help'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('For more information about bar buttons, see the <a href="!url">control panel</a> on Meebo Bar Dashboard',
    array('!url' => url('https://dashboard.meebo.com/' . variable_get('meebo_id', '') . '/settings/buttons/'))
    ) . '</p>',
  );  

 $form['meebo_sharing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Meebo Sharing'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['meebo_sharing']['help'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('For more information about meebo sharing, see the <a href="!url">control panel</a> on Meebo Bar Dashboard',
    array('!url' => url('https://dashboard.meebo.com/' . variable_get('meebo_id', '') . '/settings/sharing/'))
    ) . '</p>',
  );
  
   $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $access = user_access('use PHP for settings');
  $options = array(
    MEEBO_VISIBILITY_NOTLISTED => t('All pages except those listed'),
    MEEBO_VISIBILITY_LISTED => t('Only the listed pages'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  if (module_exists('php') && $access) {
    $options += array(MEEBO_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
    $title = t('Pages or PHP code');
    $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  else {
    $title = t('Pages');
  }

  $form['page_vis_settings']['meebo_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show Meebo Bar on specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('meebo_visibility', 0),
  );

  $form['page_vis_settings']['meebo_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('meebo_pages', ''),
    '#description' => $description,
  );
   
  return system_settings_form($form);
}